package com.seminario.prueba.Controller;


import com.seminario.prueba.Model.DocumentsModel;
import com.seminario.prueba.Service.DocumentsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/documents")
@Api(tags = "documents")
public class DocumentsController {

    @Autowired
    private DocumentsService documentsService;
    @PostMapping(path = "/insert")
    @ApiOperation(value = "Insert Documents", response = DocumentsModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public DocumentsModel insertDocuments(@RequestBody DocumentsModel documents) {
        return documentsService.saveDocuments(documents);
    }

    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Documents", response = DocumentsModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public DocumentsModel updateDocuments(@RequestBody DocumentsModel documents) {
        return documentsService.editDocuments(documents);
    }

    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Delete Documents", response = DocumentsModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Documents doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public void removeDocuments(@RequestParam Long id) {
        documentsService.deleteDocumentsById(id);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "Get All Documents", response = DocumentsModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Documents doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public List<DocumentsModel> getAllCasino() {
        return documentsService.getAllDocuments();
    }

    @GetMapping(path = "/id")
    @ApiOperation(value = "Get Documents by Id", response = DocumentsModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 401, message = "Access denied"),
            @ApiResponse(code = 404, message = "The Documents doesn't exist"),
            @ApiResponse(code = 401, message = "Expired or invalid JWT token")})
    public Optional<DocumentsModel> getByIdDocuments(@RequestParam Long id) {
        return documentsService.getById(id);
    }
}


