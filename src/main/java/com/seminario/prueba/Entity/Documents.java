package com.seminario.prueba.Entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="document")
public class Documents {

    public Documents(long id, String docnum, String picture, String name, String description, String city, String country,
                     Date datedoc, boolean status, String contactnumber, String place, String type) {

        this.id = id;
        this.docnum = docnum;
        this.picture = picture;
        this.name = name;
        this.description = description;
        this.city = city;
        this.country = country;
        this.datedoc = datedoc;
        this.status = status;
        this.contactnumber = contactnumber;
        this.place = place;
        this.type = type;
    }

    public Documents() {}

    @Id
    @GeneratedValue
    @Column(name="id", nullable=false)
    private long id;

    @Column(name="docnum", nullable=false, unique=true)
    private String docnum;

    @Column(name="picture", nullable=false)
    private String picture;

    @Column(name="name", nullable=false)
    private String name;

    @Column(name="description", nullable=false)
    private String description;

    @Column(name="city", nullable=false)
    private String city;

    @Column(name="country", nullable=false)
    private String country;

    @Column(name="datedoc", nullable=false)
    private Date datedoc;

    @Column(name="status", nullable=false)
    private boolean status;

    @Column(name="contactnumber", nullable=false)
    private String contactnumber;

    @Column(name="place", nullable=false)
    private String place;

    @Column(name="type", nullable=false)
    private String type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDocnum() {
        return docnum;
    }

    public void setDocnum(String docnum) {
        this.docnum = docnum;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getDatedoc() {
        return datedoc;
    }

    public void setDatedoc(Date datedoc) {
        this.datedoc = datedoc;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getContactnumber() {
        return contactnumber;
    }

    public void setContactnumber(String contactnumber) {
        this.contactnumber = contactnumber;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}


