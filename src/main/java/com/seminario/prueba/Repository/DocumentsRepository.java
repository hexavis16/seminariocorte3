package com.seminario.prueba.Repository;

import com.seminario.prueba.Model.DocumentsModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentsRepository extends JpaRepository <DocumentsModel, Long> {
}
