package com.seminario.prueba.Service;


import com.seminario.prueba.Model.DocumentsModel;
import com.seminario.prueba.Repository.DocumentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DocumentsService {
    @Autowired
    private DocumentsRepository documentsRepository;

    public DocumentsModel saveDocuments(DocumentsModel documents) {
        return documentsRepository.save(documents);
    }

    public DocumentsModel editDocuments(DocumentsModel documents) {
        return documentsRepository.save(documents);
    }

    public void deleteDocumentsById(Long id) {
        documentsRepository.deleteById(id);
    }

    public List<DocumentsModel> getAllDocuments() {
        return documentsRepository.findAll();
    }

    public Optional<DocumentsModel> getById(Long id) {
        return documentsRepository.findById(id);
    }
}
