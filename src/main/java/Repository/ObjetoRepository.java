package Repository;

import Model.Objeto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ObjetoRepository extends JpaRepository <Objeto, Long> {
}
